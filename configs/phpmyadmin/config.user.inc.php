<?php

/*
 * Servers configuration
 */
$i = 0;

/*
 * First server
 */
$i++;
$cfg['Servers'][$i]['blowfish_secret'] = 'k9mTWW5Tq4NDaBf3rk'; /* YOU MUST FILL IN THIS FOR COOKIE AUTH! */
/* Authentication type */
$cfg['Servers'][$i]['auth_type'] = 'cookie';
/* Server parameters */
$cfg['Servers'][$i]['compress'] = false;
$cfg['Servers'][$i]['AllowNoPassword'] = false;

$cfg['MaxNavigationItems'] = 1000;
$cfg['CheckConfigurationPermissions'] = false;
