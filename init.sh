#!/bin/bash
echo "Updating Server"

apt-get update
apt-get install -y \
    mc \
    git \
    linux-image-extra-$(uname -r) \
    linux-image-extra-virtual \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get update
apt-get install -y docker-ce
curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

#Docker post-installation steps for Linux
groupadd docker
usermod -aG docker $USER

mkdir -p ~/.ssh/
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDQPFG5COKFaVWmQWSl1AaDypN8pMNK0OK3E7XBi+SiGiNgKQw2zxCc6EcJhO1A47NSWjmR2KrFbdKkcOD+VBl6zfYwVap2T/0j3A8A9BhIH1D/zopn8gZ1tNoKWRq6B/uhVlPaehp7Q+rFutvWTLXSy0xnVML9i24rkuTEojGZXXKU9dufolKI9RRCO6FAfIiRs3klCOSXGYX8aoha+JwE/V2aDUdxJ/S5reqWWSiMExAJhaAhq1tspEVY7uuk72kHk4P7ZFv1sZz9W2bM2uus/5h8Lm0SiQsdBIE+NKdBiwJFjLUab71N2KbKBZHxR/U5FFxsyQU9Fm1SiAz5ThdH root@306' >> ~/.ssh/id_rsa.pub

echo '-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA0DxRuQjihWlVpkFkpdQGg8qTfKTDStDitxO1wYvkohojYCkM
Ns8QnOhHCYTtQOOzUlo5kdiqxW3SpHDg/lQZes32MFWqdk/9I9wPAPQYSB9Q/86K
Z/IGdbTaClkaugf7oVZT2noae0Pqxbrb1ky10stMZ1TC/YtuK5LkxKIxmV1ylPXb
n6JSiPUUQjuhQHyIkbN5JQjklxmF/GqIWvicBP1dmg1HcSf0ua3qllkojBMQCYWg
IatbbKRFWO7rpO9pB5OD+2Rb9bGc/VtmzNrrrP+YfC5tEokLHQSBPjSnQYsCRYy1
Gm+9TdimygWR8Uf1ORRcbMkFPRZtUogM+U4XRwIDAQABAoIBAQCoEgdj8WfjhkE7
D0zNC3S2qqqfz9xQZoiWe7IXpXHD5JTo6DiThE1EV7X+WOullCMINQqCTP6ImzuI
vhLbM8Xc+J/cI0iSdb07VEJH4rw2ehBGZ84OV6ofFvlloMWRoR/X+HV1wVfeZdL4
BPmq3f9jQ/NG84LuYbtqMAW1L7QYs38uz/X/+2DO91iGZKYDUqv13DHrYdsBEnLq
Ltbn7V9lktTpoetfMX7CiQuCS+JBuYlRmrPbW1KY6Pwk5ppyLRCks6cRzEU/4lNl
kQMdDWms5tD4o04d7JUl89Cx284el44+DMsD5jl1y4DUVFbnW0OJNpFF7b0N0EoN
YzGg2ZcBAoGBAOjjNqhPV+vv/ESbz3HSBm4qIVivWk9w1A3Hj9D5uJG+YtNr9h3i
IiyZ0hUXTEMpbf/C/ucD6TeVutZgCuuChi8aDJQkf48sqLIgCq8vNaQ3e5uk2yl2
H4TSszkqleHVHwI53jJtnj7/k/O+VvjUF5F/E89/bcVQFVaU/2UdLq0DAoGBAOTm
y0HLU/+2vrLa7+tnrrYplVdm36U95r9O3TBgw8yLvggbPZ+QbnvU3Sjqkl8zvVMg
GVvHHNOE2ieCjoP2s4zaJcbFw7N7kC4x2lrH2FqJGq+38YIsKDtl/u+eUrrrY0Pt
0NkC/1kAWEIlcV6/tus3Ugnslj2f0H/A4HjCg89tAoGALjE7GyE5uDOOnvEksVfO
LLWwjY2iZRVlCiKmFPYjNM1FftfwJjc/Pgd4kvRfDQc/qTY9Jwcwj/Tj7Td5kwL9
qIQLMKsYngZmo4YVeG/tvNfAbbvCOLQURl0gbVtUBUonb2KY/vmdJLO+3d7IE2zG
C/bfsA1/cV6UDOtayOyD4I8CgYAqP27EdgLK76/C0yX1DO3yMTqfJx2gIgy0wpmQ
lEuPNTudw2lcSgwaTu+C3cimSaZn8US868MmRx78q9wh/yakmJ7gUSANwICcBheH
KduE2klSpWCt5fiG/gxPrakXVtq7buldJHB8vNONYdvK4Reh0lQSANi6BwU0jyTf
JQevAQKBgER3uO/OBGrkSZ+xtUhLGIhtT/nkO0O+fx7MD8ubjZQ1CrafFpONqAyU
h9I3NrZ9IaErnCM58yXq4OCrC5Pag0SzYZ/op9Y9ycvWeILMZgMovz/QV9ZW433c
u3JnH/SLQHUgvy4vvVUNR4B5qpg/l8BTX66VFCCSihjdU3I8jtfT
-----END RSA PRIVATE KEY-----' >> ~/.ssh/id_rsa

curl -L https://github.com/drone/drone-cli/releases/download/v0.8.3/drone_linux_amd64.tar.gz | tar zx
sudo install -t /usr/local/bin drone

mkdir -p ~/tools/
cd ~/tools/
git clone git@bitbucket.org:devkstudio/drone-server.git
git clone git@bitbucket.org:devkstudio/ci-services.git

docker network create nginx-proxy
